
#!/usr/bin/python

queries = {
    "inbound_throughput": "SELECT AVG(inbound_throughput), (Timestamp/?)*?*1000 as ts FROM fw_counters WHERE Timestamp >= ? AND Timestamp <= ? GROUP BY ts LIMIT ?",
    "total_tcp_established_conns": "SELECT AVG(total_tcp_established_conns), (Timestamp/?)*?*1000 AS ts FROM fw_counters WHERE Timestamp >= ? AND Timestamp <= ? GROUP BY ts LIMIT ?",
    "used_virt_mem":  "SELECT AVG(used_virt_mem), (Timestamp/?)*?*1000 AS ts FROM fw_memory WHERE Timestamp >= ? AND Timestamp <= ? GROUP BY ts LIMIT ?",
    "cpu0":  "SELECT AVG(cpu_usage), (Timestamp/?)*?*1000 AS ts FROM UM_STAT_UM_CPU_UM_CPU_ORDERED_TABLE WHERE name_of_cpu=0 AND Timestamp >= ? AND Timestamp <= ? GROUP BY ts LIMIT ?",
    "cpu1":  "SELECT AVG(cpu_usage), (Timestamp/?)*?*1000 AS ts FROM UM_STAT_UM_CPU_UM_CPU_ORDERED_TABLE WHERE name_of_cpu=1 AND Timestamp >= ? AND Timestamp <= ? GROUP BY ts LIMIT ?"}

print queries.keys()

from flask import Flask, request
import json
from datetime import datetime

import sqlite3, os
from flask import g

app = Flask(__name__)

DATABASE = '/data/CPViewDB.dat'

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        fd = os.open(DATABASE, os.O_RDONLY)
        db = g._database = sqlite3.connect('/dev/fd/%d' % fd)
        os.close(fd)
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()





### convert Grafana datetime string to unix timestamp (seconds)
### Grafana datetime string input: str '2015-12-22T07:15:43.230Z'
### unix timestamp output: int 1450768543
def strToTimestamp(sDate):
  #~ 2015-12-22T07:15:43.230Z
  datetime_object = datetime.strptime(sDate, '%Y-%m-%dT%H:%M:%S.%fZ')
  #~ get seconds since epoch
  secTimestamp = int(((datetime_object - datetime(1970,1,1)).total_seconds()))
  return secTimestamp

@app.route("/", methods=['GET', 'POST'])
def root():
    app.logger.debug("/")
    app.logger.debug(request)
    return "OK"

@app.route("/search", methods=['POST'])
def search():
    app.logger.debug("search")
    app.logger.debug(request.json)  
    targets = queries.keys()
    return (json.dumps(targets, indent=4))

@app.route("/query", methods=['POST'])
def query():
    app.logger.debug("search")
    app.logger.debug(request.json)
    print request.json
    print strToTimestamp(request.json["range"]["from"])
    dataFrom = request.json["range"]["from"]
    print strToTimestamp(request.json["range"]["to"])
    dataTo = request.json["range"]["to"]
    print request.json["intervalMs"]
    intervalS = request.json["intervalMs"]/1000
    limit = request.json["maxDataPoints"]
    print request.json["maxDataPoints"]
    print request.json["targets"]
    print "intervalS %d limit %d" % (intervalS, limit)
    cur = get_db().cursor()
    data_series = []
    for t in request.json["targets"]:
        q = t["target"]
        print q
        datetimeFrom = strToTimestamp(dataFrom)
        print datetimeFrom
        datetimeTo = strToTimestamp(dataTo)
        print datetimeTo
        data = []
        query = queries[q]
        print query
        #cur.execute("SELECT datetime(Timestamp/1*1,'unixepoch','localtime') as ts, avg(alloc_operations) FROM kernel_memory_hmem WHERE Timestamp >= ? AND Timestamp <= ? GROUP BY (Timestamp/1) LIMIT 5", (datetimeFrom, datetimeTo, limit))
        cur.execute(query, (intervalS, intervalS, datetimeFrom, datetimeTo, limit))
        data = [list(elem) for elem in cur.fetchall()]
        #print json.dumps(data)
        target_series = {"target": str(q), "datapoints": list(data)}
        data_series.append(target_series)
    #print(json.dumps(data_series, indent=2)) 

    return json.dumps(data_series)

if __name__ == '__main__':
    app.run(host="0.0.0.0")
